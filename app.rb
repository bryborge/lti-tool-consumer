#
# LTI Tool Consumer App
#

# Gem Dependencies
require 'digest/md5'
require 'ims/lti'
require 'oauth/request_proxy/rack_request' # must include the oauth proxy object
require 'sinatra'

# Enable Sessions
enable :sessions
set :session_secret, 'secret'

#
# Throw OAuth Error
#
def throw_oauth_error
  response['WWW-Authenticate'] = "OAuth realm=\"http://#{request.env['HTTP_HOST']}\""
  throw(:halt, [401, "Not authorized\n"])
end

#
# Check Nonce
#
def was_nonce_used_in_last_x_minutes?(nonce, minutes = 60)
  # some kind of caching solution should be used to keep a record of used nonces
  # for a short amount of time.
  false
end

# Routes
require './routes.rb'
